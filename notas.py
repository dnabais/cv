creds = lambda n: n*7.5 / 20
creds(18)
# OUT: 6.75
notas = [16,17,18,18,18,18,18,18]
totalCreds = map(creds,notas)
totalCreds
# OUT: [6.0, 6.375, 6.75, 6.75, 6.75, 6.75, 6.75, 6.75]
totalCreds = sum(totalCreds)
totalCreds
# OUT: 52.875
totalCreds * 20 / 60
# OUT: 17.625
creds50 = lambda n: n*50 / 20
creds50 = lambda n: n*50.0 / 20
creds50(17)
# OUT: 42.5
creds(17)
# OUT: 6.375
creds25 = lambda n: n*2.5 / 20
creds50(17) + creds(17) + creds25(10)
# OUT: 50.125
(totalCreds + creds50(17) + creds(17) + creds25(10))*20 / 120
# OUT: 17.166666666666668
(totalCreds + creds50(17) + creds(17) + creds25(20))*20 / 120
# OUT: 17.375
(totalCreds + creds50(17) + creds(18) + creds25(20))*20 / 120
# OUT: 17.4375
(totalCreds + creds50(17) + creds(19) + creds25(20))*20 / 120
# OUT: 17.5
(totalCreds + creds50(18) + creds(17) + creds25(20))*20 / 120
# OUT: 17.791666666666668
(totalCreds + creds50(18) + creds(17) + creds25(10))*20 / 120
# OUT: 17.583333333333332
print (totalCreds + creds50(18) + creds(16) + creds25(15))*20 / 120
print (totalCreds + creds(16) + creds25(15))*20 / (120-50)
